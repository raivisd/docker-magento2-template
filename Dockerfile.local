ARG PHP_VERSION=7.2
FROM registry.kube.indvp.com/library/m2fullstack:${PHP_VERSION}
LABEL maintainer="Jurijs Jegorovs <jurijs@scandiweb.com>"
LABEL authors="Jurijs Jegorovs admin@scandiweb.com; Ilja Lapkovskis info@scandiweb.com"

# Set bash by default
SHELL ["/bin/bash", "-c", "-e", "-u", "-o", "pipefail"]

# Set permissions for non privileged users to use stdout/stderr
RUN chmod augo+rwx /dev/stdout /dev/stderr /proc/self/fd/1 /proc/self/fd/2

# Default configuration, override in deploy/local.env for localsetup
# Do not remove variables, build depends on them,
# just add "" to empty variable, the latest stable version will used instead

# These arguments are defaults, to override them, use .env
# The list of ENVs must be matched to corresponfing ARGs, to persist values in runtime
ARG PROJECT_TAG=local
ARG BASEPATH=/var/www/public
ARG COMPOSER_HOME=/tmp/composer
ARG COMPOSER_VERSION=latest
ARG COMPOSER_ALLOW_SUPERUSER=1
ARG NODEJS_DIR=/var/lib/node
ARG NODEJS_VERSION=lts
ARG NPM_CONFIG_LOGLEVEL=warn
ARG RBENV_VERSION=2.6.0
ARG RBENV_ROOT=/var/lib/ruby
ARG DOCKER_DEBUG=false
ARG INSTALL_SAMPLE_DATA=false

ENV DEBIAN_FRONTEND=noninteractive \
    DOCKER_DEBUG=${DOCKER_DEBUG} \
    CPU_CORES=$(nproc) \
    COMPOSER_ALLOW_SUPERUSER=${COMPOSER_ALLOW_SUPERUSER} \
    COMPOSER_HOME=${COMPOSER_HOME} \
    INSTALL_SAMPLE_DATA=${INSTALL_SAMPLE_DATA}\
    MAKE_OPTS="-j $CPU_CORES" \
    N_PREFIX=${NODEJS_DIR} \
    PATH=${COMPOSER_HOME}/vendor/bin:${NODEJS_DIR}/bin:${BASEPATH}/bin:${RBENV_ROOT}/shims:${RBENV_ROOT}/bin:/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

# Set working directory so any relative configuration or scripts wont fail
WORKDIR $BASEPATH

# Update server packages to latest versions
RUN apt-get update -qq \
    && apt-get dist-upgrade -y

ENV GOSU_VERSION 1.10

# Copy PHP configs
COPY deploy/shared/conf/php/php.ini /usr/local/etc/php/php.ini

# Clean up APT and temp when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Install PHP Composer
RUN printf "\033[1;34mInstalling php composer\033[0m\n"; \
    export EXPECTED_SIGNATURE=$(wget -O - -o /dev/null  https://composer.github.io/installer.sig); \
    wget -nc -O composer-setup.php https://getcomposer.org/installer; \
    printf "\033[1;34mChecking php composer signature\033[0m\n"; \
    echo "$EXPECTED_SIGNATURE" composer-setup.php | sha384sum -c - ; \
    \
    if [ -n "$COMPOSER_VERSION" ] && [ "$COMPOSER_VERSION" != "latest" ]; then \
      COMPOSER_VERSION_OVERRIDE="--version=$COMPOSER_VERSION"; \
    else COMPOSER_VERSION_OVERRIDE=""; \
    fi; \
    printf "\033[1;34mInstalling php composer\033[0m\n"; \
    php composer-setup.php --no-ansi --install-dir=/usr/local/bin --filename=composer $COMPOSER_VERSION_OVERRIDE; \
    rm composer-setup.php; \
    printf "\033[1;34mListing php composer global configuration:\033[0m\n"; \
    composer config --global data-dir $(echo $COMPOSER_HOME); \
    composer config --global vendor-dir $(echo $COMPOSER_HOME)/vendor; \
    composer config --global --list; \
    # Ensure cache folder is available \
    # Install prestissimo, multithread install helper \
    printf "\033[1;34mInstalling composer multithread plugin\033[0m\n"; \
    composer global require hirak/prestissimo;

# Recheck Ruby version, install if nondefault is set
RUN printf "\033[1;34mInstalling rbenv and ruby\033[0m\n"; \
    # Install ruby \
    rbenv install -s $RBENV_VERSION; \
    rbenv global $RBENV_VERSION; \
    \
    rbenv rehash; \
    # Production install gems skipping ri and rdoc \
    echo "gem: --no-ri --no-rdoc" >> /root/.gemrc;

# NodeJS install with n manager
RUN printf "\033[1;34mInstalling n, node version manager\033[0m\n"; \
    printf "\033[1;34mThe n home dir is: $NODEJS_DIR\n"; \
    # Fetch the n manager \
    wget --quiet -nc -O n-install https://raw.githubusercontent.com/mklement0/n-install/stable/bin/n-install; \
    # Expose varibles to env \
    export N_PREFIX="$NODEJS_DIR"; [[ :$PATH: == *":$N_PREFIX/bin:"* ]] || PATH+=":$N_PREFIX/bin"; \
    # Check prerequisites for nodejs and install it \
    bash -s n-install -t; \
    bash -s n-install -q $NODEJS_VERSION;\
    # This checks if correct node version already exists and switches to one set in NODEJS_VERSION \
    if [[ -x "$(command -v node)" ]] || [[ ! $(node -v | grep -q $NODEJS_VERSION) ]]; \
    then \
      printf "\033[1;34mInstalling nodejs $NODEJS_VERSION\033[0m\n"; \
      n $NODEJS_VERSION; \
    else \
      printf "\033[1;34mExisting node already installed, skipping\033[0m\n"; \
    fi; \
    #Ensure that latest npm is installed \
    npm install npm -g;

# Start script, executed upon container creation from image
COPY scripts/. /init/.
# Override copy inside, 
COPY deploy/shared/custom/. /init/custom/.
COPY deploy/$PROJECT_TAG/custom/. /init/custom/.

RUN chmod +x /init/start /init/entrypoint /init/wait-for-it; \
    chmod -R ugoa+rwX /tmp/composer;

ENTRYPOINT ["/init/entrypoint"]

#ENTRYPOINT ["/entrypoint.sh"]
# Spaces in command arguments will result fail to start, put each argument in "" delimited by ,
#CMD ["arg1", "arg2", ""]
CMD ["/init/start"]

# Print all versions for verification
RUN printf "\n\033[1;32mphp, composer check\033[0m\n";\
    composer diagnose || true; printf "\n";\
    printf "\n\033[1;32mnodejs, npm check\033[0m\n";\
    npm doctor; printf "\n";\
    printf "\n\033[1;32mruby, bundler check\033[0m\n";\
    bundle env; printf "\n";

RUN printf "\n\033[1;32mLocal setup is succefully built\033[0m\n";\
    printf "\033[0;33mNote: this build does not contain project files, you need to mount them in %s\n\033[0m" "$BASEPATH"