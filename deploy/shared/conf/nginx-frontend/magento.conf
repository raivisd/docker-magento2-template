## Example configuration:
# upstream fastcgi_backend {
#    # use tcp connection
#    # server  127.0.0.1:9000;
#    # or socket
#    server   unix:/var/run/php5-fpm.sock;
#    server   unix:/var/run/php/php7.0-fpm.sock;
# }
# server {
#    listen 80;
#    server_name mage.dev;
#    set $MAGE_ROOT /var/www/magento2;
#    include /vagrant/magento2/nginx.conf.sample;
# }
#
## Optional override of deployment mode. We recommend you use the
## command 'bin/magento deploy:mode:set' to switch modes instead.
##
## set $MAGE_MODE default; # or production or developer
##
## If you set MAGE_MODE in server config, you must pass the variable into the
## PHP entry point blocks, which are indicated below. You can pass
## it in using:
##
## fastcgi_param  MAGE_MODE $MAGE_MODE;
##
## In production mode, you should uncomment the 'expires' directive in the /static/ location block

real_ip_header X-Forwarded-For;
set_real_ip_from 10.0.0.0/8;

upstream fastcgi_backend {
  # use tcp connection
  server app:9000;
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name _;
    ssl_certificate /etc/nginx/cert/green-rabbit-local.crt;
    ssl_certificate_key /etc/nginx/cert/green-rabbit-local.key;

    set_real_ip_from 127.0.0.1;
    real_ip_header X-Forwarded-For;
    real_ip_recursive on;

    location / {
        proxy_pass http://frontend:3003;
        proxy_set_header Host            $host;
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_set_header X-Forwarded-Proto $scheme;
    }

    location ~* /graphql {
        proxy_pass http://middleware:3000;
        proxy_set_header Host            $host;
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_set_header X-Forwarded-Proto $scheme;
    }

    location ~* /(pwacompat.min.js|sw.js|static|media|admin/) {
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_pass http://varnish:80;
        proxy_set_header Host            $host;
        proxy_set_header X-Forwarded-For $remote_addr;
    }
}

server {
  listen 80;
  server_name _;
  set $MAGE_ROOT /var/www/public;
  ssl_certificate /etc/nginx/cert/green-rabbit-local.crt;
  ssl_certificate_key /etc/nginx/cert/green-rabbit-local.key;

  resolver 127.0.0.11;

  root $MAGE_ROOT/pub;

  index index.php;
  autoindex off;
  charset UTF-8;
  client_max_body_size 64M;
  error_page 404 403 = /errors/404.php;

  location / {
    try_files $uri $uri/ /index.php$is_args$args;
  }
  #add_header "X-UA-Compatible" "IE=Edge";

  #location ~* /(graphql|admin/) {
  #    try_files $uri $uri/ /index.php$is_args$args;
  #}

  location /pub/ {
      location ~ ^/pub/media/(downloadable|customer|import|theme_customization/.*\.xml) {
          deny all;
      }
      alias $MAGE_ROOT/pub/;
      add_header X-Frame-Options "SAMEORIGIN";
  }

  location /static/ {
      # Uncomment the following line in production mode
      # expires max;

      # Remove signature of the static files that is used to overcome the browser cache
      location ~ ^/static/version {
          rewrite ^/static/(version[^/]+/)?(.*)$ /static/$2 last;
      }

      # If webp image is not found try the same file name with .jpg
      location ~* \.(webp)$ {
          try_files $uri @webp-to-jpg;
      }

      location ~* \.(ico|webp|jpg|jpeg|png|gif|svg|js|css|swf|eot|ttf|otf|woff|woff2)$ {
          add_header Cache-Control "public";
          add_header X-Frame-Options "SAMEORIGIN";
          expires +1y;

          if (!-f $request_filename) {
              rewrite ^/static/?(.*)$ /static.php?resource=$1 last;
          }
      }
      location ~* \.(zip|gz|gzip|bz2|csv|xml)$ {
          add_header Cache-Control "no-store";
          add_header X-Frame-Options "SAMEORIGIN";
          expires    off;

          if (!-f $request_filename) {
             rewrite ^/static/?(.*)$ /static.php?resource=$1 last;
          }
      }
      if (!-f $request_filename) {
          rewrite ^/static/?(.*)$ /static.php?resource=$1 last;
      }
      add_header X-Frame-Options "SAMEORIGIN";
  }

  location /media/ {
      try_files $uri $uri/ /get.php$is_args$args;

      location ~ ^/media/theme_customization/.*\.xml {
          deny all;
      }

      # If webp image is not found try the same file name with .jpg
      location ~* \.(webp)$ {
          try_files $uri @webp-to-jpg;
      }

      location ~* \.(ico|webp|jpg|jpeg|png|gif|svg|js|css|swf|eot|ttf|otf|woff|woff2)$ {
          add_header Cache-Control "public";
          add_header X-Frame-Options "SAMEORIGIN";
          expires +1y;
          try_files $uri $uri/ /get.php$is_args$args;
      }
      location ~* \.(zip|gz|gzip|bz2|csv|xml)$ {
          add_header Cache-Control "no-store";
          add_header X-Frame-Options "SAMEORIGIN";
          expires    off;
          try_files $uri $uri/ /get.php$is_args$args;
      }
      add_header X-Frame-Options "SAMEORIGIN";
  }

  location /media/customer/ {
      deny all;
  }

  location /media/downloadable/ {
      deny all;
  }

  location /media/import/ {
      deny all;
  }

  # PHP entry point for main application
  location ~ (index|get|static|report|404|503|health_check)\.php$ {
      try_files $uri =404;
      fastcgi_pass   fastcgi_backend;
      fastcgi_buffers 1024 4k;

      fastcgi_param  PHP_FLAG  "session.auto_start=off \n suhosin.session.cryptua=off";
      fastcgi_param  PHP_VALUE "memory_limit=756M \n max_execution_time=18000";
      fastcgi_read_timeout 600s;
      fastcgi_connect_timeout 600s;

      fastcgi_index  index.php;
      fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
      include        fastcgi_params;
  }

  gzip on;
  gzip_disable "msie6";

  gzip_comp_level 6;
  gzip_min_length 1100;
  gzip_buffers 16 8k;
  gzip_proxied any;
  gzip_types
      text/plain
      text/css
      text/js
      text/xml
      text/javascript
      application/javascript
      application/x-javascript
      application/json
      application/xml
      application/xml+rss
      image/svg+xml;
  gzip_vary on;

  # Banned locations (only reached if the earlier PHP entry point regexes don't match)
  location ~* (\.php$|\.htaccess$|\.git) {
      deny all;
  }

  # Fallback to .jpg files for webp files if they are not found.
  location @webp-to-jpg {
      rewrite ^(.*)\.webp$ $1.jpg last;
  }
}
